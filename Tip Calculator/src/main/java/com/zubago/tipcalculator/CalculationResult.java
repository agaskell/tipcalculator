package com.zubago.tipcalculator;

import java.math.BigDecimal;

public class CalculationResult {
    public CalculationResult(BigDecimal tip, BigDecimal total, BigDecimal splitTip, BigDecimal splitTotal) {
        this.tip = tip;
        this.total = total;
        this.splitTip = splitTip;
        this.splitTotal = splitTotal;
    }

    private BigDecimal tip;
    private BigDecimal total;
    private BigDecimal splitTip;
    private BigDecimal splitTotal;

    public BigDecimal getTip() {
        return tip;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public BigDecimal getSplitTip() {
        return splitTip;
    }

    public BigDecimal getSplitTotal() {
        return splitTotal;
    }
}
