package com.zubago.tipcalculator;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class MainActivity extends ActionBarActivity {
    private static final String showRoundMessageKey = "showRoundMessage";
    private static final String tipPercentageKey = "tipPercentageKey";
    private static final String taxPercentageKey = "taxPercentageKey";
    private static final String taxModeKey = "taxModeKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static void setShowRoundMessage(Activity activity, boolean value) {
        SharedPreferences settings = activity.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(showRoundMessageKey, value);
        editor.commit();
    }

    private static boolean getShowRoundMessage(Activity activity) {
        SharedPreferences settings = activity.getPreferences(MODE_PRIVATE);
        return settings.getBoolean(showRoundMessageKey, true);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements TextWatcher {
        private TipCalculation tipCalculation = new TipCalculation();

        private EditText totalWithTaxText;
        private EditText taxText;
        private EditText discountText;
        private EditText splitText;
        private EditText tipPercentageText;

        private TextView tipAmountText;
        private TextView tipAmountHeader;
        private TextView totalAmountWithTipText;

        private TextView splitTipAmountText;
        private TextView splitTotalAmountWithTipText;

        private LinearLayout tipAmountContainer;
        private LinearLayout tipTotalContainer;
        private LinearLayout splitTipAmountContainer;
        private LinearLayout splitTipTotalContainer;
        private TextView roundMessageClose;
        private TextView roundDownIcon;

        private TextView taxHeader;


        private Button toggleTaxButton;

        public PlaceholderFragment() {
        }

        private void setSharedPreferences(TipCalculation tipCalculation) {
            SharedPreferences settings = getActivity().getPreferences(MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(tipPercentageKey, tipPercentageText.getText().toString());
            String tax = TaxMode.Percentage.equals(tipCalculation.getTaxMode())
                    ? taxText.getText().toString()
                    : "";
            editor.putString(taxPercentageKey, tax);
            editor.putInt(taxModeKey, tipCalculation.getTaxMode().ordinal());
            editor.commit();
        }

        private void getSharedPreferences() {
            SharedPreferences settings = getActivity().getPreferences(MODE_PRIVATE);
            tipPercentageText.setText(settings.getString(tipPercentageKey, ""));
            taxText.setText(settings.getString(taxPercentageKey, ""));
            TaxMode mode = TaxMode.values()[settings.getInt(taxModeKey, 0)];
            setTaxMode(mode);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            final View v = getView();

            roundMessageClose = (TextView) v.findViewById(R.id.round_message_close);
            roundMessageClose.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        hideAnimation(R.id.round_message_container);
                        setShowRoundMessage(getActivity(), false);
                    }
                    return false;
                }
            });

            tipAmountContainer = (LinearLayout) v.findViewById(R.id.tip_amount_container);
            tipAmountContainer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        tipCalculation.toggleRoundTip();
                        checkCalculations();
                    }
                    return false;
                }
            });

            tipTotalContainer = (LinearLayout) v.findViewById(R.id.tip_total_container);
            tipTotalContainer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        tipCalculation.toggleRoundTotal();
                        checkCalculations();
                    }
                    return false;
                }
            });

            splitTipAmountContainer = (LinearLayout) v.findViewById(R.id.split_tip_amount_container);
            splitTipAmountContainer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        tipCalculation.toggleRoundSplitTip();
                        checkCalculations();
                    }
                    return false;
                }
            });

            splitTipTotalContainer = (LinearLayout) v.findViewById(R.id.split_tip_total_container);
            splitTipTotalContainer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        tipCalculation.toggleRoundSplitTotal();
                        checkCalculations();
                    }
                    return false;
                }
            });


            totalWithTaxText = (EditText) v.findViewById(R.id.total_before_tip_text);
            totalWithTaxText.addTextChangedListener(this);

            taxText = (EditText) v.findViewById(R.id.tax_percentage_text);
            taxText.addTextChangedListener(this);

            discountText = (EditText) v.findViewById(R.id.discount_text);
            discountText.addTextChangedListener(this);

            splitText = (EditText) v.findViewById(R.id.split_text);
            splitText.addTextChangedListener(this);

            tipPercentageText = (EditText) v.findViewById(R.id.percentage_text);
            tipPercentageText.addTextChangedListener(this);

            tipAmountText = (TextView) v.findViewById(R.id.tip_amount_text);
            totalAmountWithTipText = (TextView) v.findViewById(R.id.total_amount_with_tip_text);

            tipAmountHeader = (TextView) v.findViewById(R.id.tip_amount_header);

            splitTipAmountText = (TextView) v.findViewById(R.id.split_tip_amount_text);
            splitTotalAmountWithTipText = (TextView) v.findViewById(R.id.split_total_amount_with_tip_text);


            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fontawesome-webfont.ttf");
            roundMessageClose.setTypeface(font);

            roundDownIcon = (TextView)v.findViewById(R.id.round_down_icon);
            roundDownIcon.setTypeface(font);

            taxHeader = (TextView)v.findViewById(R.id.tax_header);

            toggleTaxButton = (Button)v.findViewById(R.id.toggle_tax_button);
            toggleTaxButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View button) {
                    AnimationUtility.collapseVertical(taxHeader, 300, false, null);

                    AnimationUtility.collapseVertical(taxText, 300, false, new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            if (tipCalculation.getTaxMode().equals(TaxMode.Percentage)) {
                                setTaxMode(TaxMode.Amount);
                            } else {
                                setTaxMode(TaxMode.Percentage);
                            }

                            taxText.setText("");
                            showAnimation(R.id.tax_percentage_text, 300, new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    taxText.requestFocus();
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            AnimationUtility.expandVertical(taxHeader, 300, null);
                            checkCalculations();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                }
            });
            toggleTaxButton.setTypeface(font);

            getSharedPreferences();
        }

        private void setTaxMode(TaxMode mode) {
            if(mode.equals(TaxMode.Amount)) {
                tipCalculation.setTaxMode(TaxMode.Amount);
                taxHeader.setText(R.string.tax_amount_header);
                taxText.setHint(R.string.tax_amount_hint);
                toggleTaxButton.setText(R.string.percent);
            } else {
                tipCalculation.setTaxMode(TaxMode.Percentage);
                taxHeader.setText(R.string.tax_percent_header);
                taxText.setHint(R.string.tax_percent_hint);
                toggleTaxButton.setText(R.string.fa_money);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        @Override
        public void onStart() {
            super.onStart();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        private void showAnimation(int viewId) {
            showAnimation(viewId, 750, null);
        }

        private void hideAnimation(int viewId) {
            hideAnimation(viewId, 750, null);
        }

        private void showAnimation(int viewId, int duration, Animation.AnimationListener listener) {
            AnimationUtility.expandVertical(getActivity().findViewById(viewId), duration, listener);
        }

        private void hideAnimation(int viewId, int duration, Animation.AnimationListener listener) {
            AnimationUtility.collapseVertical(getActivity().findViewById(viewId), duration, true, listener);
        }

        private boolean isViewVisible(int viewId) {
            return getActivity().findViewById(viewId).getVisibility() == View.VISIBLE;
        }

        private boolean isShowingRoundHint() {
            return isViewVisible(R.id.round_message_container);
        }

        private boolean isShowingTotals() {
            return isViewVisible(R.id.footer);
        }

        private boolean isShowingSplitTotals() {
            return isViewVisible(R.id.split_footer);
        }

        private void updateResult(CalculationResult result) {
            BigDecimal tip = result.getTip();
            BigDecimal total = result.getTotal();
            BigDecimal splitTip = result.getSplitTip();
            BigDecimal splitTotal = result.getSplitTotal();

            DecimalFormat df = new DecimalFormat("#.00");
            this.tipAmountHeader.setText(String.format("Tip amount @ %s%%",
                    this.tipPercentageText.getText().toString()));

            if(tip != null)
                this.tipAmountText.setText(df.format(tip));

            if(total != null)
                this.totalAmountWithTipText.setText(df.format(total));

            if(splitTip != null)
                this.splitTipAmountText.setText(df.format(splitTip));

            if(splitTotal != null)
                this.splitTotalAmountWithTipText.setText(df.format(splitTotal));

            int highlightColor = getResources().getColor(R.color.holo_blue_light);
            int normalColor = getResources().getColor(R.color.holo_blue_dark);

            tipAmountContainer.setBackgroundColor(normalColor);
            tipTotalContainer.setBackgroundColor(normalColor);
            splitTipAmountContainer.setBackgroundColor(normalColor);
            splitTipTotalContainer.setBackgroundColor(normalColor);

            if(tipCalculation.getRoundTip()) {
                tipAmountContainer.setBackgroundColor(highlightColor);
            }

            if(tipCalculation.getRoundTotal()) {
                tipTotalContainer.setBackgroundColor(highlightColor);
            }

            if(tipCalculation.getRoundSplitTip()) {
                splitTipAmountContainer.setBackgroundColor(highlightColor);
            }

            if(tipCalculation.getRoundSplitTotal()) {
                splitTipTotalContainer.setBackgroundColor(highlightColor);
            }
        }

        private void checkCalculations() {
            if(tipCalculation.canCalculateTotals()) {
                updateResult(tipCalculation.calculateTotals());

                if(!isShowingTotals()) {
                    showAnimation(R.id.footer);
                    if(getShowRoundMessage(getActivity()) && !isShowingRoundHint()) {
                        showAnimation(R.id.round_message_container);
                    }
                }

                boolean isShowingSplit = isShowingSplitTotals();
                if(tipCalculation.isSplit()) {
                    if(!isShowingSplit) {
                        showAnimation(R.id.split_footer);
                    }
                } else if(isShowingSplit) {
                    hideAnimation(R.id.split_footer);
                }
            } else if(isShowingTotals()) {
                hideAnimation(R.id.footer);

                if(isShowingRoundHint()) {
                    hideAnimation(R.id.round_message_container);
                }

                if(isShowingSplitTotals()) {
                    hideAnimation(R.id.split_footer);
                }
            }
            setSharedPreferences(tipCalculation);
        }

        @Override
        public void afterTextChanged(Editable s) {
            updateValues();
            checkCalculations();
        }

        private void updateValues() {
            tipCalculation.setTotalWithTax(totalWithTaxText.getText().toString());
            tipCalculation.setTax(taxText.getText().toString());
            tipCalculation.setDiscountAmount(discountText.getText().toString());
            tipCalculation.setNumberOfPeopleSplitting(splitText.getText().toString());
            tipCalculation.setTipPercentage(tipPercentageText.getText().toString());
        }

    }

}