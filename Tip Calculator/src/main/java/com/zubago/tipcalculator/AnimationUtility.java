package com.zubago.tipcalculator;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public final class AnimationUtility {
    public static void expandVertical(final View v, int duration, Animation.AnimationListener listener) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new AlphaAnimation((float)0.0, (float)1.0)
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                super.applyTransformation(interpolatedTime, t);
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        if(listener != null) {
            a.setAnimationListener(listener);
        }

        // 1dp/ms
        a.setDuration(duration);
        v.startAnimation(a);
    }

    public static void expand(final View v, Animation.AnimationListener listener) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();
        expandVertical(v, (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density), listener);
    }

    public static void collapseVertical(final View v, int duration, final boolean hide, Animation.AnimationListener listener) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new AlphaAnimation((float)1.0, (float)0.0)
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                super.applyTransformation(interpolatedTime, t);
                if(interpolatedTime == 1 && hide){
                    v.setVisibility(View.INVISIBLE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        if(listener != null) {
            a.setAnimationListener(listener);
        }
        a.setDuration(duration);
        v.startAnimation(a);
    }

    public static void collapse(final View v, boolean hide, Animation.AnimationListener listener) {
        final int initialHeight = v.getMeasuredHeight();
        collapseVertical(v, (int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density), hide, listener);
    }
}