package com.zubago.tipcalculator;

public enum TaxMode {
    Percentage,
    Amount
}
