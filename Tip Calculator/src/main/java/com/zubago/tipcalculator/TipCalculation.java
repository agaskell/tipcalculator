package com.zubago.tipcalculator;

import android.content.SharedPreferences;

import java.math.BigDecimal;

public class TipCalculation {
    private BigDecimal mTotalWithTax;
    private BigDecimal mTax;
    private BigDecimal mDiscountAmount;
    private BigDecimal mNumberOfPeopleSplitting;
    private BigDecimal mTipPercentage;

    private TaxMode mTaxMode = TaxMode.Percentage;
    private boolean mRoundTotal;
    private boolean mRoundSplitTotal;
    private boolean mRoundTip;
    private boolean mRoundSplitTip;

    private BigDecimal getField(String value) {
        return getField(value, 2);
    }

    private BigDecimal getField(String value, int scale) {
        if(value == null || value.isEmpty()) {
            return null;
        }
        return new BigDecimal(Double.parseDouble(value)).setScale(scale, BigDecimal.ROUND_HALF_UP);
    }

    public void setTotalWithTax(String totalWithTax) {
        mTotalWithTax = getField(totalWithTax);
    }

    public TaxMode getTaxMode() {
        return mTaxMode;
    }

    public void setTaxMode(TaxMode taxMode) {
        mTaxMode = taxMode;
    }

    public void setTax(String tax) {
        mTax = getField(tax);
    }

    public void setDiscountAmount(String discountAmount) {
        mDiscountAmount = getField(discountAmount);
    }

    public void setNumberOfPeopleSplitting(String numberOfPeopleSplitting) {
        mNumberOfPeopleSplitting = getField(numberOfPeopleSplitting, 0);
    }

    public void setTipPercentage(String tipPercentage) {
        mTipPercentage = getField(tipPercentage, 3);
    }

    public void toggleRoundTotal() {
        mRoundSplitTip = false;
        mRoundSplitTotal = false;
        mRoundTip = false;
        mRoundTotal = !mRoundTotal;
    }

    public void toggleRoundTip() {
        mRoundSplitTip = false;
        mRoundSplitTotal = false;
        mRoundTotal = false;
        mRoundTip = !mRoundTip;
    }

    public void toggleRoundSplitTotal() {
        mRoundSplitTip = false;
        mRoundTip = false;
        mRoundTotal = false;
        mRoundSplitTotal = !mRoundSplitTotal;
    }

    public void toggleRoundSplitTip() {
        mRoundSplitTotal = false;
        mRoundTip = false;
        mRoundTotal = false;
        mRoundSplitTip = !mRoundSplitTip;
    }

    public boolean getRoundTip() {
        return mRoundTip;
    }

    public boolean getRoundTotal() {
        return mRoundTotal;
    }

    public boolean getRoundSplitTip() {
        return mRoundSplitTip;
    }

    public boolean getRoundSplitTotal() {
        return mRoundSplitTotal;
    }

    public boolean canCalculateTotals() {
        return this.mTipPercentage != null &&
                this.mTotalWithTax != null;
    }

    public boolean isSplit() {
        return this.mNumberOfPeopleSplitting != null &&
                this.mNumberOfPeopleSplitting.intValue() > 1;
    }

    private BigDecimal getRoundedTipValue(BigDecimal tip) {
        tip = tip.setScale(0, BigDecimal.ROUND_HALF_UP);
        if(tip.intValue() < 1) {
            tip = new BigDecimal(1);
            return tip.setScale(0, BigDecimal.ROUND_HALF_UP);
        }
        return tip;
    }

    private BigDecimal getTaxAmount(BigDecimal total) {
        if(mTax == null || mTaxMode == TaxMode.Amount) return mTax;
        BigDecimal one = new BigDecimal(1.0);

        return total.subtract(total.divide(one.add(
                mTax.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)), 2, BigDecimal.ROUND_HALF_UP));
    }

    public CalculationResult calculateTotals() {
        if(!canCalculateTotals()) return null;

        BigDecimal amountToTipOn = new BigDecimal(mTotalWithTax.doubleValue())
                .setScale(2, BigDecimal.ROUND_HALF_UP);


        BigDecimal taxAmount = getTaxAmount(amountToTipOn);
        if(taxAmount != null) {
            amountToTipOn = amountToTipOn.subtract(taxAmount);
        }

        if(mDiscountAmount != null) {
            amountToTipOn = amountToTipOn.add(mDiscountAmount);
        }

        if(isSplit()) {
            BigDecimal tip = mTipPercentage
                    .multiply(amountToTipOn)
                    .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
            BigDecimal total = tip.add(mTotalWithTax).setScale(2, BigDecimal.ROUND_HALF_UP);

            BigDecimal splitTip = tip.divide(mNumberOfPeopleSplitting, 2, BigDecimal.ROUND_HALF_UP);
            BigDecimal splitTotal = total.divide(mNumberOfPeopleSplitting, 2, BigDecimal.ROUND_HALF_UP);


            if(mRoundTip) {
                tip = getRoundedTipValue(tip);
                total = tip.add(mTotalWithTax).setScale(2, BigDecimal.ROUND_HALF_UP);
                splitTip = tip.divide(mNumberOfPeopleSplitting, 2, BigDecimal.ROUND_HALF_UP);
                splitTotal = total.divide(mNumberOfPeopleSplitting, 2, BigDecimal.ROUND_HALF_UP);
            }

            if(mRoundTotal) {
                total = total.setScale(0, BigDecimal.ROUND_HALF_UP);
                tip = total.subtract(mTotalWithTax);
                splitTip = tip.divide(mNumberOfPeopleSplitting, 2, BigDecimal.ROUND_HALF_UP);
                splitTotal = total.divide(mNumberOfPeopleSplitting, 2, BigDecimal.ROUND_HALF_UP);
            }

            if(mRoundSplitTotal) {
                splitTotal = splitTotal.setScale(0, BigDecimal.ROUND_HALF_UP);
                total = splitTotal.multiply(mNumberOfPeopleSplitting).setScale(2, BigDecimal.ROUND_HALF_UP);
                tip = total.subtract(mTotalWithTax).setScale(2, BigDecimal.ROUND_HALF_UP);
                splitTip = tip.divide(mNumberOfPeopleSplitting, 2, BigDecimal.ROUND_HALF_UP);
            }

            if(mRoundSplitTip) {
                splitTip = getRoundedTipValue(splitTip);
                tip = splitTip.multiply(mNumberOfPeopleSplitting).setScale(2, BigDecimal.ROUND_HALF_UP);
                total = tip.add(mTotalWithTax).setScale(2, BigDecimal.ROUND_HALF_UP);
                splitTotal = total.divide(mNumberOfPeopleSplitting, 2, BigDecimal.ROUND_HALF_UP);
            }

            return new CalculationResult(tip, total, splitTip, splitTotal);
        } else {
            BigDecimal tip = mTipPercentage.multiply(amountToTipOn)
                    .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);

            if(mRoundTip) {
                tip = getRoundedTipValue(tip);
            }

            BigDecimal total = tip.add(mTotalWithTax).setScale(2, BigDecimal.ROUND_HALF_UP);

            if(mRoundTotal) {
                total = total.setScale(0, BigDecimal.ROUND_HALF_UP);
                tip = total.subtract(mTotalWithTax);
            }

            return new CalculationResult(tip, total, null, null);
        }
    }
}
